Feature: Factories
    Background: Customer
    Given the model class is "KDA\Laravel\Domain\Models\"

    Scenario: Model can be created
    Given the following factory attributes 
    """
    {
        "name":"test"
    }
    """
    And crafting a record 
    Then The record is not null

    Scenario: Model can be created without any attribute
    Given crafting a record 
    Then The record is not null
