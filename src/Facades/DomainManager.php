<?php

namespace KDA\Laravel\Domain\Facades;

use Illuminate\Support\Facades\Facade;

class DomainManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
